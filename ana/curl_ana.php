<?php
require_once 'AnaXML.php';
//require_once 'funcoes.php';
# An HTTP GET request example
$date = new DateTime();
$dataAtual = $date->format('d/m/Y');


if( isset($_GET['estacao']) )
{
	$estacao = $_GET['estacao'];
	$dataInicio = $_GET['dataInicio'];
	$dataFim = $_GET['dataFim'];
	
}
else
{
	$estacao = '';
	$dataInicio = '';
	$dataFim = '';	
}

if(date('w') == 1)
{
	$diaAnteriorInicial = new DateTime();
	$diaAnteriorInicial->modify('-3 day');
	$diaAnteriorInicial = $diaAnteriorInicial->format('d/m/Y');

	$diaAnteriorFinal = new DateTime();
	$diaAnteriorFinal->modify('-1 day');
	$diaAnteriorFinal = $diaAnteriorFinal->format('d/m/Y');
}
else
{
	$diaAnteriorInicial = new DateTime();
	$diaAnteriorInicial->modify('-1 day');
	$diaAnteriorInicial = $diaAnteriorInicial->format('d/m/Y');

	$diaAnteriorFinal = new DateTime();
	$diaAnteriorFinal->modify('-1 day');
	$diaAnteriorFinal = $diaAnteriorFinal->format('d/m/Y');
}

$diaInicialMes = date('01/m/Y');


//$url = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$dataAtual.'&dataFim='.$dataAtual;
$urlDataAtual = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$dataAtual.'&dataFim='.$dataAtual;
$urlIntervalo = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$dataInicio.'&dataFim='.$dataFim;

$urlx = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$diaAnteriorInicial.'&dataFim='.$diaAnteriorFinal;	

$urly = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$diaInicialMes.'&dataFim='.$diaFinal;

	
#dados estacao dia atual
$dataXml = AnaXML::getEstacoesAna($urlDataAtual);
$xml = AnaXML::getAjusteXML($dataXml);
$arrayEstacao = AnaXML::getDadosHidrometeorologicosByEstacao($xml);

#dados estacao intervalo de data
$dataXml = AnaXML::getEstacoesAna($urlIntervalo);
$xml = AnaXML::getAjusteXML($dataXml);
#precipita��o do intervalo dado
$precipitacaoIntervalo = AnaXML::getPrecipitacao($xml);

//$arrayEstacaoAltamira = $funcoes->arrayEstacao('altamira');

#precipitacao dia anterior
$dataXmlAnterior = AnaXML::getEstacoesAna($urlx);
$xmlAnterior = AnaXML::getAjusteXML($dataXmlAnterior);
$precipitacaoDiaAnterior = AnaXML::getPrecipitacao($xmlAnterior);

#precipitacao acumulada do mes
$dataXmlAcumulada = AnaXML::getEstacoesAna($urly);
$xmlAcumulada = AnaXML::getAjusteXML($dataXmlAcumulada);
$precipitacaoAcumulada = AnaXML::getPrecipitacao($xmlAcumulada);

$resultPrecipitacao = getMaiorPrecipitacaoIntervalo($estacao,$dataInicio,$dataFim);
$dataMaiorPrecipitacao = $resultPrecipitacao['data'];
$valorMaiorPrecipitacao = $resultPrecipitacao['valor'];

if ($arrayEstacao->CodEstacao == ""){
	
echo"<h4 style='color:red;'>Esta��o em Manuten��o</h4>";
}else{
	if ($dataInicio == $dataFim)
		$intervaloData = $dataInicio;
	else
		$intervaloData = $dataInicio.' ao '.$dataFim;
	echo"<p><strong>Codigo Fluviometrico da Esta��o: </strong>$arrayEstacao->CodEstacao</p>
		<p><strong>N�vel para �s 07:00h do dia Atual: </strong> $arrayEstacao->Nivel</p>
		<p><strong>Chuva do dia $intervaloData: </strong> $precipitacaoIntervalo</p>
		<p><strong>Chuva do dia anterior: </strong> $precipitacaoDiaAnterior</p>
		<p><strong>Chuva acumulada (no m�s): </strong>$precipitacaoAcumulada</p>
		<p>
			<strong>
				Maior precipita��o acumulada em um dia no intervalo de $dataInicio 00:00:00 at� $dataFim 23:59:59:
			</strong>
			$valorMaiorPrecipitacao no dia $dataMaiorPrecipitacao.
		</p>";
}

//retorna a data e o valor de maior precipita��o acumulada para um dia no m�s em quest�o
function getMaiorPrecipitacaoIntervalo($codEstacao,$dataInicioString,$dataFimString){
	/*$dataAtual = new DateTime();
	$mesAtual = $dataAtual->format('m');
	$diaAtual = $dataAtual->format('d');
	$anoAtual = $dataAtual->format('Y');
	$diaInicio = $dataInicio->format('d');

	$diaFim = $dataFim->format('m');*/

	$dataInicio = DateTime::createFromFormat('d/m/Y',$dataInicioString);
	$dataFim = DateTime::createFromFormat('d/m/Y',$dataFimString);


	$maiorPrecipitacao = 0;	
	$dataMaiorPrecipitacao;
	for ($dia = $dataInicio;(int)$dataFim->diff($dia)->format('%R%d') <= 0;$dia->add(new DateInterval('P1D'))){
		//$strData = "$dia/$mesAtual/$anoAtual";
		$strData = $dia->format('d/m/Y');
		$url = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$codEstacao.'&dataInicio='.$strData.'&dataFim='.$strData;

	
		$dataXmlAcumulada = AnaXML::getEstacoesAna($url);
		$xmlAcumulada = AnaXML::getAjusteXML($dataXmlAcumulada);
		$precipitacaoAcumulada = AnaXML::getPrecipitacao($xmlAcumulada);

		if ($precipitacaoAcumulada > $maiorPrecipitacao){
			$maiorPrecipitacao = $precipitacaoAcumulada;
			$dataMaiorPrecipitacao = $strData;
		}
	}

	$resultado = array("valor" => $maiorPrecipitacao, "data" => $dataMaiorPrecipitacao);
	return $resultado;
}

if(date('w') == 1)
{
	$diaInicial = new DateTime();
	$diaInicial->modify('-3 day');
	$diaInicial = $diaInicial->format('d/m/Y');

	$diaFinal = new DateTime();
	$diaFinal->modify('-1 day');
	$diaFinal = $diaFinal->format('d/m/Y');
}
else
{
	$diaInicial = new DateTime();
	$diaInicial->modify('-1 day');
	$diaInicial = $diaInicial->format('d/m/Y');

	$diaFinal = new DateTime();
	$diaFinal->modify('-1 day');
	$diaFinal = $diaFinal->format('d/m/Y');
}


$estacoes = [ 1=>['altamira'=>18850000], 2=>['bacabal'=>33290000], 3=>['boa vista'=>14620000],
			 4=>['grajau'=>33321000] ,5=>['obidos'=>17050001], 6=>['porto velho'=>15400000], 7=>['rio branco'=>13600002],
			 8=>['santarem'=>17900000],9=>['humaita'=>15630000], 10=>['tabatinga'=>10100000],11=>['boca do acre'=>13700000],
			 12=>['manaus'=>14990000],13=>['assis brasil'=>13450000],14=>['brasileia'=>13470000],15=>['xapuri'=>13551000],
			 16=>['belem'=>31645000],17=>['maraba'=>29050000],18=>['itaituba'=>17730000],
			 20=>['parauapebas'=>29070100],21=>['oriximina'=>16900000],22=>['grajau'=>33321000],23=>['barra do corda'=>33250000],
			 24=>['joselandia'=>33273000],25=>['boa vista'=>14620000],26=>['normandia'=>14527000],27=>['bonfim'=>14528000],
			 28=>['guajara mirim'=>15250000],29=>['ji-parana'=>15560000],30=>['tefe'=>12900001],
			 31=>['boa vista'=>14620000],32=>['pacaraima'=>14530000],33=>['joselandia'=>33273000]
];

foreach ($estacoes as $e)
	$url[key($e)] = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.current($e).'&dataInicio='.$dataAtual.'&dataFim='.$dataAtual;
foreach ($estacoes as $ex)
	$urlx[key($ex)] = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.current($ex).'&dataInicio='.$diaInicial.'&dataFim='.$diaFinal;




	
//altamira

$dataXmlAltamira = AnaXML::getEstacoesAna($url['altamira']);
$xmlAltamira = AnaXML::getAjusteXML($dataXmlAltamira);
$arrayEstacaoAltamira = AnaXML::getDadosHidrometeorologicosByEstacao($xmlAltamira);


//$arrayEstacaoAltamira = $funcoes->arrayEstacao('altamira');
#altamira dia anterior

$dataXmlAltamiraAnterior = AnaXML::getEstacoesAna($urlx['altamira']);
$xmlAltamiraAnterior = AnaXML::getAjusteXML($dataXmlAltamiraAnterior);
$precipitacaoAltamira = AnaXML::getPrecipitacao($xmlAltamiraAnterior);

//$precipitacaoAltamira = $funcoes->precipitacao('altamira');

//bacabal
$dataXmlBacabal = AnaXML::getEstacoesAna($url['bacabal']);
$xmlBacabal = AnaXML::getAjusteXML($dataXmlBacabal);
$arrayEstacaoBacabal = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBacabal);

#bacabal dia anterior
$dataXmlBacabalAnterior = AnaXML::getEstacoesAna($urlx['bacabal']);
$xmlBacabalAnterior = AnaXML::getAjusteXML($dataXmlBacabalAnterior);
$precipitacaoBacabal = AnaXML::getPrecipitacao($xmlBacabalAnterior);

//boa vista
$dataXmlBoaVista = AnaXML::getEstacoesAna($url['boa vista']);
$xmlBoaVista = AnaXML::getAjusteXML($dataXmlBoaVista);
$arrayEstacaoBoaVista = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBoaVista);

#boa vista dia anterior
$dataXmlBoaVistaAnterior = AnaXML::getEstacoesAna($urlx['boa vista']);
$xmlBoaVistaAnterior = AnaXML::getAjusteXML($dataXmlBoaVistaAnterior);
$precipitacaoBoaVista = AnaXML::getPrecipitacao($xmlBoaVistaAnterior);

//grajau
$dataXmlGrajau = AnaXML::getEstacoesAna($url['grajau']);
$xmlGrajau = AnaXML::getAjusteXML($dataXmlGrajau);
$arrayEstacaoGrajau = AnaXML::getDadosHidrometeorologicosByEstacao($xmlGrajau);

#grajau dia anterior
$dataXmlGrajauAnterior = AnaXML::getEstacoesAna($urlx['grajau']);
$xmlGrajauAnterior = AnaXML::getAjusteXML($dataXmlGrajauAnterior);
$precipitacaoGrajau = AnaXML::getPrecipitacao($xmlGrajauAnterior);


//obidos
$dataXmlObidos = AnaXML::getEstacoesAna($url['obidos']);
$xmlObidos = AnaXML::getAjusteXML($dataXmlObidos);
$arrayEstacaoObidos = AnaXML::getDadosHidrometeorologicosByEstacao($xmlObidos);

#obidos dia anterior
$dataXmlObidosAnterior = AnaXML::getEstacoesAna($urlx['obidos']);
$xmlObidosAnterior = AnaXML::getAjusteXML($dataXmlObidosAnterior);
$precipitacaoObidos = AnaXML::getPrecipitacao($xmlObidosAnterior);

//porto velho
$dataXmlPortoVelho = AnaXML::getEstacoesAna($url['porto velho']);
$xmlPortoVelho = AnaXML::getAjusteXML($dataXmlPortoVelho);
$arrayPortoVelho = AnaXML::getDadosHidrometeorologicosByEstacao($xmlPortoVelho);

#porto velho dia anterior
$dataXmlPortoVelhoAnterior = AnaXML::getEstacoesAna($urlx['porto velho']);
$xmlPortoVelhoAnterior = AnaXML::getAjusteXML($dataXmlPortoVelhoAnterior);
$precipitacaoPortoVelho = AnaXML::getPrecipitacao($xmlPortoVelhoAnterior);

//rio branco
$dataXmlRioBranco = AnaXML::getEstacoesAna($url['rio branco']);
$xmlRioBranco = AnaXML::getAjusteXML($dataXmlRioBranco);
$arrayRioBranco = AnaXML::getDadosHidrometeorologicosByEstacao($xmlRioBranco);

#rio branco dia anterior
$dataXmlRioBrancoAnterior = AnaXML::getEstacoesAna($urlx['rio branco']);
$xmlRioBrancoAnterior = AnaXML::getAjusteXML($dataXmlRioBrancoAnterior);
$precipitacaoRioBranco = AnaXML::getPrecipitacao($xmlRioBrancoAnterior);


//santarem
$dataXmlSantarem = AnaXML::getEstacoesAna($url['santarem']);
$xmlSantarem = AnaXML::getAjusteXML($dataXmlSantarem);
$arrayEstacaoSanterem = AnaXML::getDadosHidrometeorologicosByEstacao($xmlSantarem);

#santarem dia anterior
$dataXmlSantaremAnterior = AnaXML::getEstacoesAna($urlx['santarem']);
$xmlSantaremAnterior = AnaXML::getAjusteXML($dataXmlSantaremAnterior);
$precipitacaoSantarem = AnaXML::getPrecipitacao($xmlSantaremAnterior);

//-Amazonas

//--Boca do Acre
$dataXmlBocaDoAcre = AnaXML::getEstacoesAna($url['boca do acre']);
$xmlBocaDoAcre = AnaXML::getAjusteXML($dataXmlBocaDoAcre);
$arrayEstacaoBocaDoAcre = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBocaDoAcre);
#boca do acre dia anterior
$dataXmlBocaDoAcreAnterior = AnaXML::getEstacoesAna($urlx['boca do acre']);
$xmlBocaDoAcreAnterior = AnaXML::getAjusteXML($dataXmlBocaDoAcreAnterior);
$precipitacaoBocaDoAcre = AnaXML::getPrecipitacao($xmlBocaDoAcreAnterior);

//--Humaita
$dataXmlHumaita = AnaXML::getEstacoesAna($url['humaita']);
$xmlHumaita = AnaXML::getAjusteXML($dataXmlHumaita);
$arrayEstacaoHumaita = AnaXML::getDadosHidrometeorologicosByEstacao($xmlHumaita);
#humaita dia anterior
$dataXmlHumaitaAnterior = AnaXML::getEstacoesAna($urlx['humaita']);
$xmlHumaitaAnterior = AnaXML::getAjusteXML($dataXmlHumaitaAnterior);
$precipitacaoHumaita = AnaXML::getPrecipitacao($xmlHumaitaAnterior);

//--Tabatinga
$dataXmlTabatinga = AnaXML::getEstacoesAna($url['tabatinga']);
$xmlTabatinga = AnaXML::getAjusteXML($dataXmlTabatinga);
$arrayEstacaoTabatinga = AnaXML::getDadosHidrometeorologicosByEstacao($xmlTabatinga);
#tabatinga dia anterior
$dataXmlTabatingaAnterior = AnaXML::getEstacoesAna($urlx['tabatinga']);
$xmlTabatingaAnterior = AnaXML::getAjusteXML($dataXmlTabatingaAnterior);
$precipitacaoTabatinga = AnaXML::getPrecipitacao($xmlTabatingaAnterior);

//--Tefe
$dataXmlTefe = AnaXML::getEstacoesAna($url['tefe']);
$xmlTefe = AnaXML::getAjusteXML($dataXmlTefe);
$arrayEstacaoTefe = AnaXML::getDadosHidrometeorologicosByEstacao($xmlTefe);
#tef� dia anterior
$dataXmlTefeAnterior = AnaXML::getEstacoesAna($urlx['tefe']);
$xmlTefeAnterior = AnaXML::getAjusteXML($dataXmlTefeAnterior);
$precipitacaoTefe = AnaXML::getPrecipitacao($xmlTefeAnterior);

//--Manaus
$dataXmlManaus = AnaXML::getEstacoesAna($url['manaus']);
$xmlManaus = AnaXML::getAjusteXML($dataXmlManaus);
$arrayEstacaoManaus = AnaXML::getDadosHidrometeorologicosByEstacao($xmlManaus);
#manaus dia anterior
$dataXmlManausAnterior = AnaXML::getEstacoesAna($urlx['manaus']);
$xmlManausAnterior = AnaXML::getAjusteXML($dataXmlManausAnterior);
$precipitacaoManaus = AnaXML::getPrecipitacao($xmlManausAnterior);


//-Acre
//--Assis Brasil
$dataXmlAssisBrasil = AnaXML::getEstacoesAna($url['assis brasil']);
$xmlAssisBrasil = AnaXML::getAjusteXML($dataXmlAssisBrasil);
$arrayEstacaoAssisBrasil = AnaXML::getDadosHidrometeorologicosByEstacao($xmlAssisBrasil);
#assis brasil dia anterior
$dataXmlAssisBrasilAnterior = AnaXML::getEstacoesAna($urlx['manaus']);
$xmlAssisBrasilAnterior = AnaXML::getAjusteXML($dataXmlAssisBrasilAnterior);
$precipitacaoAssisBrasil = AnaXML::getPrecipitacao($xmlAssisBrasilAnterior);

//--Brasileia
$dataXmlBrasileia = AnaXML::getEstacoesAna($url['brasileia']);
$xmlBrasileia = AnaXML::getAjusteXML($dataXmlBrasileia);
$arrayEstacaoBrasileia = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBrasileia);
#brasileia dia anterior
$dataXmlBrasileiaAnterior = AnaXML::getEstacoesAna($urlx['brasileia']);
$xmlBrasileiaAnterior = AnaXML::getAjusteXML($dataXmlBrasileiaAnterior);
$precipitacaoBrasileia = AnaXML::getPrecipitacao($xmlBrasileialAnterior);

//--Xapuri
$dataXmlXapuri = AnaXML::getEstacoesAna($url['xapuri']);
$xmlXapuri = AnaXML::getAjusteXML($dataXmlXapuri);
//var_dump($xmlXapuri);
$arrayEstacaoXapuri = AnaXML::getDadosHidrometeorologicosByEstacao($xmlXapuri);
//var_dump($arrayEstacaoXapuri);
//die;
#xapuri dia anterior
$dataXmlXapuriAnterior = AnaXML::getEstacoesAna($urlx['xapuri']);
$xmlXapuriAnterior = AnaXML::getAjusteXML($dataXmlXapuriAnterior);
$precipitacaoXapuri = AnaXML::getPrecipitacao($xmlXapuriAnterior);

//-Para
//--Belem
$dataXmlBelem = AnaXML::getEstacoesAna($url['belem']);
$xmlBelem = AnaXML::getAjusteXML($dataXmlBelem);
$arrayEstacaoBelem = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBelem);
#belem dia anterior
$dataXmlBelemAnterior = AnaXML::getEstacoesAna($urlx['belem']);
$xmlBelemAnterior = AnaXML::getAjusteXML($dataXmlBelemAnterior);
$precipitacaoBelem = AnaXML::getPrecipitacao($xmlBelemAnterior);

//--Maraba
$dataXmlMaraba = AnaXML::getEstacoesAna($url['maraba']);
$xmlMaraba = AnaXML::getAjusteXML($dataXmlMaraba);
$arrayEstacaoMaraba = AnaXML::getDadosHidrometeorologicosByEstacao($xmlMaraba);
#maraba dia anterior
$dataXmlMarabaAnterior = AnaXML::getEstacoesAna($urlx['maraba']);
$xmlMarabaAnterior = AnaXML::getAjusteXML($dataXmlMarabaAnterior);
$precipitacaoMaraba = AnaXML::getPrecipitacao($xmlMarabaAnterior);

//--Itaituba
$dataXmlItaituba = AnaXML::getEstacoesAna($url['itaituba']);
$xmlItaituba = AnaXML::getAjusteXML($dataXmlItaituba);
$arrayEstacaoItaituba = AnaXML::getDadosHidrometeorologicosByEstacao($xmlItaituba);
#maraba dia anterior
$dataXmlItaitubaAnterior = AnaXML::getEstacoesAna($urlx['itaituba']);
$xmlItaitubaAnterior = AnaXML::getAjusteXML($dataXmlItaitubaAnterior);
$precipitacaoItaituba = AnaXML::getPrecipitacao($xmlItaitubaAnterior);

//--Parauapebas
$dataXmlParauapebas = AnaXML::getEstacoesAna($url['parauapebas']);
$xmlParauapebas = AnaXML::getAjusteXML($dataXmlParauapebas);
$arrayEstacaoParauapebas = AnaXML::getDadosHidrometeorologicosByEstacao($xmlParauapebas);
#maraba dia anterior
$dataXmlParauapebasAnterior = AnaXML::getEstacoesAna($urlx['parauapebas']);
$xmlParauapebasAnterior = AnaXML::getAjusteXML($dataXmlParauapebasAnterior);
$precipitacaoParauapebas = AnaXML::getPrecipitacao($xmlParauapebasAnterior);

//--Oriximina
$dataXmlOriximina = AnaXML::getEstacoesAna($url['oriximina']);
$xmlOriximina = AnaXML::getAjusteXML($dataXmlOriximina);
$arrayEstacaoOriximina = AnaXML::getDadosHidrometeorologicosByEstacao($xmlOriximina);
#maraba dia anterior
$dataXmlOriximinaAnterior = AnaXML::getEstacoesAna($urlx['parauapebas']);
$xmlOriximinaAnterior = AnaXML::getAjusteXML($dataXmlOriximinaAnterior);
$precipitacaoOriximina = AnaXML::getPrecipitacao($xmlOriximinaAnterior);


//-Maranhao

//--Grajau
$dataXmlGrajau = AnaXML::getEstacoesAna($url['grajau']);
$xmlGrajau = AnaXML::getAjusteXML($dataXmlGrajau);
$arrayEstacaoGrajau = AnaXML::getDadosHidrometeorologicosByEstacao($xmlGrajau);
#grajau dia anterior
$dataXmlGrajauAnterior = AnaXML::getEstacoesAna($urlx['grajau']);
$xmlGrajauAnterior = AnaXML::getAjusteXML($dataXmlOriximinaAnterior);
$precipitacaoGrajau = AnaXML::getPrecipitacao($xmlGrajauAnterior);

//--Barra do Corda
$dataXmlBarraDoCorda = AnaXML::getEstacoesAna($url['barra do corda']);
$xmlBarraDoCorda = AnaXML::getAjusteXML($dataXmlBarraDoCorda);
$arrayEstacaoBarraDoCorda = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBarraDoCorda);
#barra do corda dia anterior
$dataXmlBarraDoCordaAnterior = AnaXML::getEstacoesAna($urlx['barra do corda']);
$xmlBarraDoCordaAnterior = AnaXML::getAjusteXML($dataXmlBarraDoCordaAnterior);
$precipitacaoBarraDoCorda = AnaXML::getPrecipitacao($xmlBarraDoCordaAnterior);

//--Joselandia
$dataXmlJoselandia = AnaXML::getEstacoesAna($url['joselandia']);
$xmlJoselandia = AnaXML::getAjusteXML($dataXmlJoselandia);
$arrayEstacaoJoselandia = AnaXML::getDadosHidrometeorologicosByEstacao($xmlJoselandia);
#joselandia dia anterior
$dataXmlJoselandiaAnterior = AnaXML::getEstacoesAna($urlx['joselandia']);
$xmlJoselandiaAnterior = AnaXML::getAjusteXML($dataXmlJoselandiaAnterior);
$precipitacaoJoselandia = AnaXML::getPrecipitacao($xmlJoselandiaAnterior);

//--Boa Vista
$dataXmlBoaVista = AnaXML::getEstacoesAna($url['boa vista']);
$xmlBoaVista = AnaXML::getAjusteXML($dataXmlBoaVista);
$arrayEstacaoBoaVista = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBoaVista);
#boa vista dia anterior
$dataXmlBoaVistaAnterior = AnaXML::getEstacoesAna($urlx['boa vista']);
$xmlBoaVistaAnterior = AnaXML::getAjusteXML($dataXmlBoaVistaAnterior);
$precipitacaoBoaVista = AnaXML::getPrecipitacao($xmlBoaVistaAnterior);

//--Normandia
$dataXmlNormandia = AnaXML::getEstacoesAna($url['normandia']);
$xmlNormandia = AnaXML::getAjusteXML($dataXmlNormandia);
$arrayEstacaoNormandia = AnaXML::getDadosHidrometeorologicosByEstacao($xmlNormandia);
#normandia do corda dia anterior
$dataXmlNormandiaAnterior = AnaXML::getEstacoesAna($urlx['normandia']);
$xmlNormandiaAnterior = AnaXML::getAjusteXML($dataXmlNormandiaAnterior);
$precipitacaoNormandia = AnaXML::getPrecipitacao($xmlNormandiaAnterior);

//--Bonfim
$dataXmlBonfim = AnaXML::getEstacoesAna($url['bonfim']);
$xmlBonfim = AnaXML::getAjusteXML($dataXmlBonfim);
$arrayEstacaoBonfim = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBonfim);
#bonfim dia anterior
$dataXmlBonfimAnterior = AnaXML::getEstacoesAna($urlx['normandia']);
$xmlBonfimAnterior = AnaXML::getAjusteXML($dataXmlBonfimAnterior);
$precipitacaoBonfim = AnaXML::getPrecipitacao($xmlBonfimAnterior);

//--Pacaraima
$dataXmlPacaraima = AnaXML::getEstacoesAna($url['pacaraima']);
$xmlPacaraima = AnaXML::getAjusteXML($dataXmlPacaraima);
$arrayEstacaoPacaraima = AnaXML::getDadosHidrometeorologicosByEstacao($xmlPacaraima);
#Pacaraima dia anterior
$dataXmlPacaraimaAnterior = AnaXML::getEstacoesAna($urlx['pacaraima']);
$xmlPacaraimaAnterior = AnaXML::getAjusteXML($dataXmlPacaraimaAnterior);
$precipitacaoPacaraima = AnaXML::getPrecipitacao($xmlPacaraimaAnterior);

//-Rondonia

//--Guajara-Mirim
$dataXmlGuajaraMirim = AnaXML::getEstacoesAna($url['guajara mirim']);
$xmlGuajaraMirim = AnaXML::getAjusteXML($dataXmlGuajaraMirim);
$arrayEstacaoGuajaraMirim = AnaXML::getDadosHidrometeorologicosByEstacao($xmlGuajaraMirim);
#J�-Paran� dia anterior
$dataXmlGuajaraMirimAnterioir = AnaXML::getEstacoesAna($urlx['guajara mirim']);
$xmlGuajaraMirimAnterior = AnaXML::getAjusteXML($dataXmlGuajaraMirimAnterioir);
$precipitacaoGuajaraMirim = AnaXML::getPrecipitacao($xmlGuajaraMirimAnterior);

//--J�-Paran�
$dataXmlJiParana = AnaXML::getEstacoesAna($url['ji-parana']);
$xmlJiParana = AnaXML::getAjusteXML($dataXmlJiParana);
$arrayEstacaoJiParana = AnaXML::getDadosHidrometeorologicosByEstacao($xmlJiParana);
#J�-Paran� dia anterior
$dataXmlJiParanaAnterioir = AnaXML::getEstacoesAna($urlx['ji-parana']);
$xmlJiParanaAnterior = AnaXML::getAjusteXML($dataXmlJiParanaAnterioir);
$precipitacaoJiparana = AnaXML::getPrecipitacao($xmlJiParanaAnterior);
?>