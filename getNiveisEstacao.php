<?php	

	function criarTabelaNiveis($dadosEstacao,$dataInicio,$dataFim){

		global $codigosEstacoes;
		$tabelaHTML = '<div  class="container">
		<div class="table-responsive">
		<table class="table">'.
			'<thead>
				<tr valign="Middle" bgcolor="Gainsboro">
					<th>Estação</th>
					<th>Data e Hora</th>
					<th>Nível (cm)</th>
				</tr>
				</thead>
				<tbody>
				 <tr>';
	
		foreach($dadosEstacao['DadosHidrometereologicos'] as $dado){
			$hora = (int)substr($dado->DataHora,11,12);
			$obtemDados = !($dataInicio == $dataFim);
			
			if ($hora >= 7)
				$obtemDados = true;
			
			if ($obtemDados){
				$colunaEstacao = "<td>".$codigosEstacoes[(int)$dado->CodEstacao]."</td>";
				$colunaDataHora = "<td>".$dado->DataHora."</td>";
				$colunaNivel = "<td>".$dado->Nivel."</td>";
				$linhaTabela = "<tr>".$colunaEstacao.$colunaDataHora.$colunaNivel."</tr>";
				$tabelaHTML .= $linhaTabela;
			}
		}

		$tabelaHTML .= "</tbody></table></div></div>";
		echo $tabelaHTML;
	}

	function getDadosGraficoNiveis($dadosEstacao,$dataInicio,$dataFim){
		$arrayNiveis = array();

		foreach($dadosEstacao['DadosHidrometereologicos'] as $dado){
			$hora = (int)substr($dado->DataHora,11,12);
			$obtemDados = !($dataInicio == $dataFim);

			if ($hora >= 7)
				$obtemDados = true;
			if ($obtemDados)
				$arrayNiveis[] = $dado->Nivel;
		}
		echo '['.implode(',',array_reverse($arrayNiveis)).']';
	}


	require_once 'ana/AnaXML.php';

	$codigosEstacoes = json_decode(file_get_contents('codigosEstacoes.json'),true);

	const TABELA = 0;
	const GRAFICO = 1;

	if($_SERVER['REQUEST_METHOD'] == 'GET')
	{
		$tipo = false;
		$estacao = false;
		$dataAtual = (new DateTime())->format('d/m/Y');
		
		if (isset($_GET['tipo'])){
			//tipo de dado: 0 para tabela e 1 para grafico
			$tipo = $_GET['tipo'];
		}

		if(isset($_GET['estacao']))
			$estacao = $_GET['estacao'];
		
		$urlDataAtual = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$_GET['dataIniciox'].'&dataFim='.$_GET['dataFimx'];

		$dataXml = AnaXML::getEstacoesAna($urlDataAtual);
		$dadosEstacao = AnaXML::getAjusteXML($dataXml);
		

		if ($tipo == TABELA)
			criarTabelaNiveis($dadosEstacao,$_GET['dataIniciox'],$_GET['dataFimx']);
		else if($tipo == GRAFICO)
			getDadosGraficoNiveis($dadosEstacao,$_GET['dataIniciox'],$_GET['dataFimx']);
	}

?>
