<!DOCTYPE html>
<!-- saved from url=(0062)http://voky.com.ua/showcase/sky-tabs/examples/layout-left.html -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Download de Dados</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    
		<link href="styles/css/bootstrap.css" rel="stylesheet" media="screen">		
		<link rel="stylesheet" href="styles/css/demo.css">
		<link rel="stylesheet" href="styles/css/font-awesome.css">
		<link rel="stylesheet" href="styles/css/sky-tabs.css">
		<script  type="text/javascript" src="funcoes.js"></script>
    
    
    <!--[if lt IE 9]>
      <link rel="stylesheet" href="css/sky-tabs-ie8.css">
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script src="js/sky-tabs-ie8.js"></script>
    <![endif]-->
  </head>
  
  <body class="bg-cyan">
    <div class="body">
    
      <!-- tabs -->
      <div class="sky-tabs sky-tabs-pos-left sky-tabs-anim-flip sky-tabs-response-to-icons">
        <input type="radio" name="sky-tabs" checked="" id="sky-tab1" class="sky-tab-content-1">
        <label for="sky-tab1"><span><span> <i class="fa fa-sun-o"></i><i class="fa fa-cloud"></i></i> INMET </span></span></label>
                
        
        <input type="radio" name="sky-tabs" id="sky-tab2" class="sky-tab-content-2">
        <label for="sky-tab2"><span><span><i class="fa fa-tint"></i> ANA </span></span></label>
        
        <ul>
          <li class="sky-tab-content-1">          
            <div class="typography">
                           
              <h1>INMET</h1>

<ul class="nav nav-tabs" > 

  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> BELÉM-PA <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="#tab_belem_A" data-toggle="tab">Automatica</a></li>
    <li><a href="#tab_belem_C" data-toggle="tab">convencional</a></li>
    </ul>
  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">BACABAL-MA <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="#tab_bacabal_A" data-toggle="tab">Automatica</a></li>
    <li><a href="#tab_bacabal_C" data-toggle="tab">convencional</a></li>
    </ul>  
  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">BOA VISTA-RR <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="#tab_boa_A" data-toggle="tab">Automatica</a></li>
    <li><a href="#tab_boa_C" data-toggle="tab">convencional</a></li>
    </ul>
  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">GRAJAÚ-MA<span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="#tab_grajau_A" data-toggle="tab">Automatica</a></li>
    </ul>
  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> ALTAMIRA-PA<span class="caret"></span></a>
    <ul class="dropdown-menu">
    <li><a href="#tab_altamira_C" data-toggle="tab">convencional</a></li>
    </ul>
  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> MANAUS-AM <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="#tab_manaus_A" data-toggle="tab">Automatica</a></li>
    <li><a href="#tab_manaus_C" data-toggle="tab">convencional</a></li>
    </ul>
  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> MARABÁ-PA <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="#tab_maraba_A" data-toggle="tab">Automatica</a></li>
    <li><a href="#tab_maraba_C" data-toggle="tab">convencional</a></li>
    </ul>  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> ÓBIDOS-PA <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="#tab_obidos_A" data-toggle="tab">Automatica</a></li>
    <li><a href="#tab_obidos_C" data-toggle="tab">convencional</a></li>
    </ul>  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">PORTO VELHO-RO <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="#tab_porto_A" data-toggle="tab">Automatica</a></li>
    </ul>
  </li>
   
</ul><!-- end of nav -->
<div class="tab-content">

        <div class="tab-pane" id="tab_belem_A">
           <br> <h4><u>BELÉM-PA</u></h4>
            <p>Estação Convencional:
            <a href="inmet/belemAutomatica.php" target="_blank"> Clique </a>
            </p>
        </div>
        <div class="tab-pane" id="tab_belem_C">
            <br> <h4><u>BELÉM-PA</u></h4>
            <p>Estação Convencional:
            <a href="inmet/belemConvencional.php" target="_blank"> Clique </a>
                </p>
        </div>
        <div class="tab-pane" id="tab_bacabal_A">
            <br><h4><u>BACABAL-MA</u></h4>
            <p>Estação automatica:
                <a href="inmet/bacabalAutomatica.php" target="_blank"> Clique </a>
            </p>
        </div>
        <div class="tab-pane" id="tab_bacabal_C">
            <br><h4><u>BACABAL-MA</u></h4>
            <p>Estação Convencional:
                <a href="inmet/bacabalConvencional.php" target="_blank"> Clique </a>
                </p>
        </div>
        <div class="tab-pane" id="tab_boa_A">
            <br><h4><u>BOA VISTA-RR</u></h4>
            <p>Estação automatica:
             <a href="inmet/boaVistaAutomatica.php" target="_blank"> Clique </a>
            </p>   
        </div>
    <div class="tab-pane" id="tab_boa_C">
            <br><h4><u>BOA VISTA-RR</u></h4>
            <p>Estação convencional:
                <a href="inmet/boaVistaConvencional.php" target="_blank"> Clique </a>
                </p>
        </div>
    <div class="tab-pane" id="tab_grajau_A">
            <br><h4><u>GRAJAÚ-MA</u></h4>
            <p>Estação automatica:
                <a href="inmet/grajauAutomatica.php" target="_blank"> Clique </a>
            </p>
        </div>
        <div class="tab-pane" id="tab_altamira_C">
            <br><h4><u>ALTAMIRA-PA</u></h4>
            <p>Estação Convencional:
                <a href="inmet/altamiraConvencional.php" target="_blank"> Clique </a>
            </p>
        </div>
    <div class="tab-pane " id="tab_manaus_A">
            <br><h4><u>MANAUS-MA</u></h4>
            <p>Estação automatica:
                <a href="inmet/manausAutomatica.php" target="_blank"> Clique </a>
        </p>
        </div>
        <div class="tab-pane" id="tab_manaus_C">
            <br><h4><u>MANAUS-MA</u></h4>
            <p>Estação Convencional:
                <a href="inmet/manausConvencional.php" target="_blank"> Clique </a>
            </p>
        </div>
     <div class="tab-pane" id="tab_maraba_A">
            <br><h4><u>MARABA-PA</u></h4>
            <p>Estação automatica:
                 <a href="inmet/marabaAutomatica.php" target="_blank"> Clique </a>
         </p>
        </div>
        <div class="tab-pane" id="tab_maraba_C">
            <br><h4><u>MARABA-PA</u></h4>
            <p>Estação Convencional:
                 <a href="inmet/marabaConvencional.php" target="_blank"> Clique </a>
            </p>
        </div>
     <div class="tab-pane" id="tab_obidos_A">
            <br><h4><u>ÓBIDOS-PA</u></h4>
            <p>Estação automatica:
                 <a href="inmet/obidosAutomatica.php" target="_blank"> Clique </a>
         </p>
        </div>
        <div class="tab-pane" id="tab_obidos_C">
            <br><h4><u>ÓBIDOS-PA</u></h4>
            <p>Estação Convencional:
                 <a href="inmet/obidosConvencional.php" target="_blank"> Clique </a>
            </p>
        </div>
    <div class="tab-pane" id="tab_porto_A">
            <br><h4><u>PORTO VELHO-RO</u></h4>
            <p>Estação automatica:
                <a href="inmet/portoVelhoAutomatica.php" target="_blank"> Clique </a>
        </p>
        </div> 
        
<!-- tab content -->
                            <p align="justify">Nesta aba e possivel fazer o download de dados do INMET. O Instituto Nacional de Meteorologia do Brasil (INMET) é um órgão federal da administração direta do Ministério da Agricultura, Pecuária e Abastecimento (MAPA), criado em 1909 com a missão de prover informações meteorológicas através de monitoramento, análise e previsão do tempo e clima, concorrendo com processos de pesquisa aplicada para prover informações adequadas em situações diversas, como no caso de desastres naturais como inundações e secas extremas que afetam, limitam ou interferem nas atividades cotidianas da sociedade brasileira. </p>
              
              <p class="text-right"><em>Fonte dos dados: <a href="http://www.inmet.gov.br/portal/" target="_blank">Portal INMET</a>.</em></p>
            </div>
          </li>
          
          <li class="sky-tab-content-2">
            <div class="typography">
              <h1>ANA - Agência Nacional de Água</h1>
              
                              
  <ul class="nav nav-tabs" >
      <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">AMAZONAS<span class="caret"></span></a>
    <ul class="dropdown-menu">
		<!-- 13700000 é o codigo da estação de Boca do Acre -->
		<li><a href="#tab_boca_acre" data-toggle="tab" onclick="get_ana('13700000')">Boca do Acre</a></li>
		<!-- 15630000 é o codigo da estação de Humaitá -->
		<li><a href="#tab_humaita" data-toggle="tab" onclick="get_ana('15630000')">Humaitá</a></li>
		<!-- 10100000 é o codigo da estação de Tabatinga -->
		<li><a href="#tab_tabatinga" data-toggle="tab" onclick="get_ana('10100000')">Tabatinga</a></li>
		<!-- 12900001 é o codigo da estação de Tefé -->
        <li><a href="#tab_tefe" data-toggle="tab" onclick="get_ana(12900001)">Tefé</a></li>
		<!-- 14990000 é o codigo da estação de MANAUS -->
        <li><a href="#tab_manaus" data-toggle="tab" onclick="get_ana(14990000)" >Manaus</a></li>
    
    </ul><!-- end of dropdown menu -->
          
    <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">ACRE<span class="caret"></span></a>
    <ul class="dropdown-menu">
		<!-- 13600002 é o codigo da estação de Rio Branco -->
		<li><a href="#tab_rio" data-toggle="tab" onclick="get_ana(13600002)">Rio Branco</a></li>
		<!-- 13450000 é o codigo da estação de Assis Brasil -->
		<li><a href="#tab_ass" data-toggle="tab" onclick="get_ana(13450000)">Assis Brasil</a></li>
		<!-- 13470000 é o codigo da estação de Brasiléia -->
		<li><a href="#tab_br" data-toggle="tab" onclick="get_ana(13470000)">Brasiléia</a></li>
		<!-- 13551000 é o codigo da estação de Xapúri -->
		<li><a href="#tab_xapu" data-toggle="tab" onclick="get_ana(13551000)">Xapúri</a></li>
       
    </ul><!-- end of dropdown menu -->
  </li>  
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> PARA <span class="caret"></span></a>
    <ul class="dropdown-menu">
		<!-- 18850000 é o codigo da estação de Altamira -->
		<li><a href="#tab_altamira" data-toggle="tab" onclick="get_ana(18850000)">Altamira</a></li>
		<!-- 31645000 é o codigo da estação de Belém -->
		<li><a href="#tab_belem" data-toggle="tab" onclick="get_ana(31645000)">Belém</a></li>
		<!-- 29050000 é o codigo da estação de Marabá -->
		<li><a href="#tab_maraba" data-toggle="tab" onclick="get_ana(29050000)">Marabá</a></li>
		<!-- 17730000 é o codigo da estação de Itaituba -->
		<li><a href="#tab_itaituba" data-toggle="tab" onclick="get_ana(17730000)">Itaituba</a></li>
		<!-- 17900000 é o codigo da estação de Santarem -->
		<li><a href="#tab_santarem" data-toggle="tab" onclick="get_ana(17900000)">Santarem</a></li>
		<!-- 29070100 é o codigo da estação de Parauapebas -->
		<li><a href="#tab_parauapebas" data-toggle="tab" onclick="get_ana(29070100)">Parauapebas</a></li>
		<!-- 17050001 é o codigo da estação de Óbidos -->
		<li><a href="#tab_obidos" data-toggle="tab" onclick="get_ana(17050001)">Óbidos</a></li>
		<!-- 16900000 é o codigo da estação de Oriximiná -->
		<li><a href="#tab_oriximina" data-toggle="tab" onclick="get_ana(16900000)">Oriximiná</a></li>
    </ul><!-- end of dropdown menu -->    
  </li>
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> MARANHÃO <span class="caret"></span></a>
    <ul class="dropdown-menu">
		<!-- 33290000 é o codigo da estação de Oriximiná -->
		<li><a href="#tab_bacabal" data-toggle="tab" onclick="get_ana(33290000)">Bacabal</a></li>
		<!-- 33321000 é o codigo da estação de Grajaú -->
		<li><a href="#tab_grajau" data-toggle="tab" onclick="get_ana(33321000)">Grajaú</a></li>
		<!-- 33250000 é o codigo da estação de Barra do Corda -->
		<li><a href="#tab_barra" data-toggle="tab" onclick="get_ana(33250000)">Barra do Corda</a></li>
		<!-- 33273000 é o codigo da estação de Joselândia -->
		<li><a href="#tab_jose" data-toggle="tab" onclick="get_ana(33273000)">Joselândia</a></li>
    </ul><!-- end of dropdown menu -->
  </li>   
      <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">RORAIMA<span class="caret"></span></a>
    <ul class="dropdown-menu">
		<!-- 14620000 é o codigo da estação de Boa Vista -->
		<li><a href="#tab_boa" data-toggle="tab" onclick="get_ana(14620000)">Boa Vista</a></li>
		<!-- 14527000 é o codigo da estação de Normândia -->
		<li><a href="#tab_normandia" data-toggle="tab" onclick="get_ana(14527000)">Normândia</a></li>
		<!-- 14528000 é o codigo da estação de Bonfim -->
        <li><a href="#tab_bonfim" data-toggle="tab" onclick="get_ana(14528000)">Bonfim</a></li>
		<!-- 14530000 é o codigo da estação de Pacaraima -->
        <li><a href="#tab_paca" data-toggle="tab" onclick="get_ana(14530000)">Pacaraima</a></li>
    </ul><!-- end of dropdown menu -->
  </li>  
      <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">RONDONIA<span class="caret"></span></a>
    <ul class="dropdown-menu">
		<!-- 15400000 é o codigo da estação de Porto Velho -->
		<li><a href="#tab_porto" data-toggle="tab" onclick="get_ana(15400000)">Porto velho</a></li>
		<!-- 15250000 é o codigo da estação de Guajará-Mirim -->
		<li><a href="#tab_mirin" data-toggle="tab" onclick="get_ana(15250000)">Guajará-Mirim</a></li>
		<!-- 15560000 é o codigo da estação de Ji-Paraná -->
        <li><a href="#tab_ji" data-toggle="tab" onclick="get_ana(15560000)">Ji-Paraná</a></li>
    </ul><!-- end of dropdown menu -->
          
  </li>
</ul><!-- end of nav -->

<!--tab content - ANA -->
<div class="tab-content">
	 <div class="tab-pane" id="tab_boca_acre" >
		<h4>Boca do Acre</h4>
		<div class="tab-pane" id="13700000" ></div>
	</div>
	
	<div class="tab-pane" id="tab_humaita">
		<h4>Humaitá</h4>
		<div class="tab-pane" id="15630000" ></div>
	</div>
	
	<div class="tab-pane" id="tab_tabatinga">
		<h4>Tabatinga</h4>
		<div class="tab-pane" id="10100000"></div>
	</div>
	
	<div class="tab-pane" id="tab_tefe">
		<h4>Tefé</h4>
		<div class="tab-pane" id="12900001"></div>
	</div>
	
	<div class="tab-pane" id="tab_manaus">
		<h4>Manaus</h4>
		<div class="tab-pane" id="14990000"></div>
	</div>
	
	<!--Acre-->
	<div class="tab-pane" id="tab_rio">
		<h4>Rio Branco</h4>
		<div class="tab-pane" id="13600002"></div>
	</div>
	
	<div class="tab-pane" id="tab_ass">
		<h4>Assis Brasil</h4>
		<div class="tab-pane" id="13450000"></div>
	</div>
	
	<div class="tab-pane" id="tab_br">
		<h4>Brasiléia</h4>
		<div class="tab-pane" id="13470000"></div>
	</div>
	
	<div class="tab-pane" id="tab_xapu">
		<h4>Xapúri</h4>
		<div class="tab-pane" id="13551000"></div>
	</div>
	
	<!--Pará-->
	
	<div class="tab-pane" id="tab_altamira">
		<h4>Altamira</h4>
		<div class="tab-pane" id="18850000"></div>
	</div>
	
	<div class="tab-pane" id="tab_belem">
		<h4>Belém</h4>
		<div class="tab-pane" id="31645000"></div>
	</div>
	
	<div class="tab-pane" id="tab_maraba">
		<h4>Marabá</h4>
		<div class="tab-pane" id="29050000"></div>
	</div>
	
	<div class="tab-pane" id="tab_itaituba">
		<h4>Itaituba</h4>
		<div class="tab-pane" id="17730000"></div>
	</div>
	
	<div class="tab-pane" id="tab_santarem">
		<h4>Santarém</h4>
		<div class="tab-pane" id="17900000"></div>
	</div>
	
	<div class="tab-pane" id="tab_parauapebas">
		<h4>Parauapebas</h4>
		<div class="tab-pane" id="29070100"></div>
	</div>	
	
	<div class="tab-pane" id="tab_obidos">
		<h4>Óbidos</h4>
		<div class="tab-pane" id="17050001"></div>
	</div>
	
	<div class="tab-pane" id="tab_oriximina">
		<h4>Oriximiná</h4>
		<div class="tab-pane" id="16900000"></div>
	</div>
	<!--end Para-->
	
	<!--MARANHÃO-->
	<div class="tab-pane" id="tab_bacabal">
		<h4>Bacabal</h4>
		<div class="tab-pane" id="33290000"></div>
	</div>
	
	<div class="tab-pane" id="tab_grajau">
		<h4>Grajaú</h4>
		<div class="tab-pane" id="33321000"></div>
	</div>
	
	<div class="tab-pane" id="tab_barra">
		<h4>Barra do Corda</h4>
		<div class="tab-pane" id="33250000"></div>
	</div>
	
	<div class="tab-pane" id="tab_jose">
		<h4>Joselândia</h4>
		<div class="tab-pane" id="33273000"></div>
	</div>
	
	<div class="tab-pane" id="tab_boa">
		<h4>Boa Vista</h4>
		<div class="tab-pane" id="14620000"></div>
	</div>
	
	<div class="tab-pane" id="tab_normandia">
		<h4>Normândia</h4>
		<div class="tab-pane" id="14527000"></div>
	</div>
	
	<div class="tab-pane" id="tab_bonfim">
		<h4>Bonfim</h4>
		<div class="tab-pane" id="14528000"></div>
	</div>
	<div class="tab-pane" id="tab_paca">
		<h4>Pacaraima</h4>
		<div class="tab-pane" id="14530000"></div>
	</div>	
	<!--end MARANHÃO-->
	
	<!-- RONDONIA -->
	
	<div class="tab-pane" id="tab_porto">
		<h4>Porto Velho</h4>
		<div class="tab-pane" id="15400000"></div>
	</div>	
	
	<div class="tab-pane" id="tab_mirin">
		<h4>Guajará-Mirim</h4>
		<div class="tab-pane" id="15250000"></div>
	</div>	
	
	<div class="tab-pane" id="tab_ji">
		<h4>Jí Paraná</h4>
		<div class="tab-pane" id="15560000"></div>
	</div>	
	
</div>

<!-- end of tab content ANA-->
							<p><strong><u>Informações sobre os Dados informados:</u></strong></p>
                            <p align="justify">Nesta seção é possível visualizarmos os dados das estações monitoradas da ANA pelo projeto SINTEGRA, os dados são capturados pelo proprio webservice da ANA .</p>
							<p>Os dados capturados são:</p>
							<ul>
								<li>Nome da Estação</li>
								<li>Código Pluviometrico e Fluviometrico da Estação</li>
							    <li>Nível: Nível da Bacia monitorada pela estação no Horário de 07:00 h do dia vigente,</li>
								<li>Chuva: Quantidade acumulada de Chuva para o dia vigente. Caso seja segunda-feira considera-se o somatório do acumulado do Sexta, Sábado e Domingo anterior</li>
							</ul>
							<p><strong><u>Sobre a ANA</u></strong></p>
							<p>A Agência Nacional de Águas (ANA) é uma autarquia federal, vinculada ao Ministério do Meio Ambiente, e responsável pela implementação da gestão dos recursos hídricos brasileiros.</p>
							<p>Tem como missão regulamentar o uso das águas dos rios e lagos de domínio da União e implementar o Sistema Nacional de Gerenciamento
							de Recursos Hídricos, garantindo o seu uso sustentável, evitando a poluição e o desperdício, e assegurando água de boa qualidade e em quantidade suficiente para a atual e as futuras gerações.</p>
							
            
              <p class="text-right"><em>Fonte do Dados: <a href="http://www2.ana.gov.br/Paginas/default.aspx" target="_blank">Portal ANA</a>.</em></p>
            </div>
          </li>
                  
        </ul>
      </div>
      <!--/ tabs -->
      
    </div>
  
    <script src="styles/js/jquery.min.js"></script>
    <script src="styles/js/bootstrap.min.js"></script>
</body></html>
