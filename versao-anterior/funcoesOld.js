var dataInicio, dataFim;
var codigoAtualEstacaoANA = null;
var codigosEstacoes,grafico;



const TABELA = 0,GRAFICO = 1;

$(document).ready(function(){

	$('.linkEstacao').click(function(){
		let idTabAtiva = $('.ui-tabs-active').prop('id'),tipo;

		switch(idTabAtiva){
			case 'tabTabela':
				tipo = 0;
				break;
			case 'tabGrafico':
				tipo = 1;
				break;
		}
		carregarConteudoTab(tipo);
	});

	$('.linkTab').click(function(){
		let tipo,idLink = $(this).prop('id');

		switch(idLink){
			case 'linkTabela':
				tipo = TABELA;
				break;
			case 'linkGrafico':
				tipo = GRAFICO;
				break;	
		}

		if (!codigoAtualEstacaoANA){
		}
		else
			carregarConteudoTab(tipo);
	});

	$.getJSON("codigosEstacoes.json",function(data){
		codigosEstacoes = data;
	});

	$('#tabs-ui').tabs({
		create: function(event,ui){},
		beforeActivate: function(event,ui){
			if (codigoAtualEstacaoANA)
				ui.newPanel.html('');
		}
	});

	$('#datepicker').datepicker({
		todayHighlight: true,
		language: "pt-BR",
		format: 'dd/mm/yyyy',
		startDate: new Date((new Date()).getTime() - 31536000000),
		endDate: new Date()
	});
	
	atualizarDatas();
	$('#datepicker').datepicker().on('changeDate',function(e){atualizarDatas(e)});

});

function get_ana(estacao){
	codigoAtualEstacaoANA = estacao;
	
	$.ajax({
		type: 'get',
		url: '/dados/ana/curl_ana.php?estacao=' 
			+ estacao + '&dataInicio=' + dataInicio + '&dataFim=' + dataFim,
		dataType: 'html',
		contentType: 'charset=utf-8',
		success: function(data){
			document.getElementById(estacao).innerHTML = data;
		}
	});
}

function mostrarDiv(tipoEstacao,codigo){
	var p = $('#displayInfoEstacao p'),tipo;

	atualizarDatas();

	if (tipoEstacao == 0)
		tipo = "automática"
	else if(tipoEstacao == 1)
		tipo = "convencional"
			
	p.html(p.html().replace(/\s(.*):/,' ' + tipo + ':'));


	$('#displayInfoEstacao p a').prop('href',"inmet/dadosEstacao.php?tipoEstacao=" + tipoEstacao + "&codigo=" + codigo + '&dataInicio=' + dataInicio + '&dataFim=' + dataFim);
}

//obtem as datas selecionadas pelo datepicker e retorna um array de duas posições([dataInicio,dataFim])
function atualizarDatas(e){
	console.log("ATUALIZAR DATAS");
	dataInicio = $("#datepickerInicio").datepicker('getDate');
	dataFim = $("#datepickerFim").datepicker('getDate');

	
	//Tratamento das datas informadas
	//se forem vazias, a data atual é passada como data de fim e início
	//as datas são convertidas para uma string no formato dd/mm/yyyyq
	if (dataInicio)
		dataInicio = new Date(dataInicio);
	else
		dataInicio = new Date();
	dataInicio = dataInicio.toLocaleDateString('pt-BR');

	if (dataFim)
		dataFim = new Date(dataFim);
	else
		dataFim = new Date();
	dataFim = dataFim.toLocaleDateString('pt-BR');

	
	
	var a = $('#displayInfoEstacao p a'),
		link = a.attr('href');	
	if (link && link != ""){
		//atualizar parâmetros 'dataInicio' e 'dataFim' da requisição no link
		var datas,posDataInicio,posDataFim;
		//obtem as strings que correspondem respectivamente às datas inicial e final
		datas = link.match(/\d\d\/\d\d\/\d\d\d\d/g);
		posDataInicio = link.indexOf(datas[0]);//posição da data inicial na string do link
		posDataFim = link.indexOf(datas[1],posDataInicio + 1);//posição da data final na string do link

		//atualiza as novas datas 'dataInicio' e 'dataFim' no link
		a.prop('href',link.substring(0,posDataInicio) + dataInicio + link.substring(posDataInicio + 10,posDataFim) + dataFim);
	}
	
}

function carregarConteudoTab(tipo){
	$.ajax({
		url:'getNiveisEstacao.php?estacao=' + codigoAtualEstacaoANA + '&tipo=' + tipo,
		success:function(data){
			let conteudo;	
			if (tipo == TABELA){
				conteudo = $('#conteudoTabela');
				conteudo.html(data);
			}
			else if (tipo == GRAFICO){
				conteudo = $('#conteudoGrafico');
				inicializarGrafico('conteudoGrafico',data);
			}

			conteudo.css('display', 'none');
			conteudo.fadeIn(1000);
		}

	});
}


function inicializarGrafico(idContainer,stringArrayNiveis){
	let arrayNiveis = eval(stringArrayNiveis);
	let dataHoje = new Date();

	if (grafico){
	
		//grafico ja foi inicializado
		/*grafico.update({
			title: {
				text: 'Nível da Bacia - ' + codigosEstacoes[codigoAtualEstacaoANA]
			},
			series: [{
				name: 'Níveis',
				data: arrayNiveis
		    	}]
		});*/
		grafico.setTitle({text: codigosEstacoes[codigoAtualEstacaoANA]},false);
		grafico.series[0].setData(arrayNiveis);
	}
	else{
	
		//grafico ainda não foi inicializado
		let dataHoje = new Date();
			

		grafico = Highcharts.chart(idContainer, {
		    title: {
			text: 'Nível da Bacia - ' + codigosEstacoes[codigoAtualEstacaoANA]
		    },

		    subtitle: {
			text: 'Fonte: Agência Nacional de Águas - ANA'
		    },

		    yAxis: {
			title: {
			    text: 'Nível(cm)'
			},
			
		    },
		     xAxis: {
			type: 'datetime'
		    },
		    legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		    },

		    plotOptions: {
		    line: {
			    dataLabels: {
				enabled: true
			    },
			    enableMouseTracking: false,
			    pointInterval: 900000, 
			    pointStart: new Date(
						dataHoje.getFullYear(),
						dataHoje.getMonth(),
						dataHoje.getDate(),
						4	
					).getTime()
			}
		    },

		    series: [{
			name: 'Níveis',
			data: arrayNiveis
		    }]
		});
	}

//	console.log(grafico.userOptions.plotOptions);
}

