var dataInicio, dataFim;
var codigoAtualEstacaoANA = null;
var codigosEstacoes,grafico;



const TABELA = 0,GRAFICO = 1;

//retorna TABELA se a tab ativa for da tabela, e GRAFICO se a tab ativa for do gráfico
function getTipoTabAtiva(){
	let idTabAtiva = $('.ui-tabs-active').prop('id'),
		tipo;

	switch(idTabAtiva){
		case 'tabTabela':
			tipo = TABELA;
			break;
		case 'tabGrafico':
			tipo = GRAFICO;
			break;
	}
	return tipo;
}

$(document).ready(function(){

	$('.conteudoTab').on('atualizaConteudo',function(event){
		let tipo;
		switch($(this).prop('id')){
			case 'conteudoTabela':
				tipo = TABELA;
				break;
			case 'conteudoGrafico':
				tipo = GRAFICO;
				break;

		}
		carregarConteudoTab(tipo);
		$('#atualizarConteudo').prop('disabled',true);
	});

	/*$('.linkEstacao').click(function(){
		let tipo = getTipoTabAtiva();
		$('.linkTab').click(function(){
			setTimeout(function(){
				$('.conteudoTab[aria-hidden="false"]').trigger('atualizaConteudo');
			},0);
		});
		carregarConteudoTab(tipo);
	});*/

	
	

/*	$('.linkTab').click(function(){
		let tipo ,idLink = $(this).prop('id');

		switch(idLink){
			case 'linkTabela':
				tipo = TABELA;
				break;
			case 'linkGrafico':
				tipo = GRAFICO;
				break;	
		}

		if (codigoAtualEstacaoANA)
			carregarConteudoTab(tipo);
	});*/

	$('.linkTab, .linkEstacao, #atualizarConteudo').click(function(){
		if (codigoAtualEstacaoANA)
			setTimeout(function(){
				$('.conteudoTab[aria-hidden="false"]').trigger('atualizaConteudo');
			},0);
	});

	

	$.getJSON("codigosEstacoes.json",function(data){
		codigosEstacoes = data;
	});

	$('#tabs-ui').tabs({
		create: function(event,ui){}
	});

	$('#datepicker').datepicker({
		todayHighlight: true,
		language: "pt-BR",
		format: 'dd/mm/yyyy',
		startDate: new Date((new Date()).getTime() - 31536000000),
		endDate: new Date()
	});
	
	atualizarDatas();
	$('#datepicker').datepicker().on('changeDate',function(e){

		atualizarDatas(e);

		$('#atualizarConteudo').prop('disabled',false);
	
	});

});

function get_ana(estacao){
	codigoAtualEstacaoANA = estacao;
	
	$.ajax({
		type: 'get',
		url: './ana/curl_ana.php?estacao=' 
			+ estacao + '&dataInicio=' + dataInicio + '&dataFim=' + dataFim,
		dataType: 'html',
		contentType: 'charset=utf-8',
		success: function(data){
			document.getElementById(estacao).innerHTML = data;
		}
	});
}

function mostrarDiv(tipoEstacao,codigo){
	var p = $('#displayInfoEstacao p'),tipo;

	atualizarDatas();

	if (tipoEstacao == 0)
		tipo = "automática"
	else if(tipoEstacao == 1)
		tipo = "convencional"
			
	p.html(p.html().replace(/\s(.*):/,' ' + tipo + ':'));


	$('#displayInfoEstacao p a').prop('href',"inmet/dadosEstacao.php?tipoEstacao=" + tipoEstacao + "&codigo=" + codigo + '&dataInicio=' + dataInicio + '&dataFim=' + dataFim);
}

//obtem as datas selecionadas pelo datepicker e retorna um array de duas posições([dataInicio,dataFim])
function atualizarDatas(e){
	dataInicio = $("#datepickerInicio").datepicker('getDate');
	dataFim = $("#datepickerFim").datepicker('getDate');

	
	//Tratamento das datas informadas
	//se forem vazias, a data atual é passada como data de fim e início
	//as datas são convertidas para uma string no formato dd/mm/yyyyq
	if (dataInicio)
		dataInicio = new Date(dataInicio);
	else
		dataInicio = new Date();
	dataInicio = dataInicio.toLocaleDateString('pt-BR');

	if (dataFim)
		dataFim = new Date(dataFim);
	else
		dataFim = new Date();
	dataFim = dataFim.toLocaleDateString('pt-BR');

	
	
	var a = $('#displayInfoEstacao p a'),
		link = a.attr('href');	
	if (link && link != ""){
		//atualizar parâmetros 'dataInicio' e 'dataFim' da requisição no link
		var datas,posDataInicio,posDataFim;
		//obtem as strings que correspondem respectivamente às datas inicial e final
		datas = link.match(/\d\d\/\d\d\/\d\d\d\d/g);
		posDataInicio = link.indexOf(datas[0]);//posição da data inicial na string do link
		posDataFim = link.indexOf(datas[1],posDataInicio + 1);//posição da data final na string do link

		//atualiza as novas datas 'dataInicio' e 'dataFim' no link
		a.prop('href',link.substring(0,posDataInicio) + dataInicio + link.substring(posDataInicio + 10,posDataFim) + dataFim);
	}
	
}

function animarCarregando(id){
	let tamanhoSlide = 500;

	$('#' + id).children('span').html('Carregando...');
	return timerID = setTimeout(function callback(){
		$('#' + id + " span").animate({
			marginLeft: '' + tamanhoSlide
		},500);
		if (tamanhoSlide)
			tamanhoSlide = 0;
		else
			tamanhoSlide = 500;
		setTimeout(callback,600);
	},0);
}

function carregarConteudoTab(tipo){
	let conteudo,texto,textoTamanhoInicial,timerID;

	if (tipo == TABELA)
		conteudo = $('#conteudoTabela');
	else if (tipo == GRAFICO)
		conteudo = $('#conteudoGrafico');

	conteudo.html('');
	texto = $('<div id="carregando"><span></span></div>').appendTo(conteudo);

	timerID = animarCarregando('carregando');

	$.ajax({
		url:'getNiveisEstacao.php?estacao=' + codigoAtualEstacaoANA + '&tipo=' + tipo + '&dataIniciox=' + dataInicio + '&dataFimx=' + dataFim,
		beforeSend: function(xhr){
		},
		cache: true,
		success:function(data){
			conteudo.html('');
			if (tipo == TABELA)
				conteudo.html(data);
			else if (tipo == GRAFICO)
				inicializarGrafico('conteudoGrafico',data);
			
			clearTimeout(timerID);

			conteudo.css('display', 'none');
			conteudo.fadeIn(1000);

			//mostra botão de atualização de dados que está escondido
			$('#atualizarConteudo').fadeIn(1000);
		}	
	});

}

//retorna as datas correspondentes aos extremos iniciais e finais do eixo X do gráfico
function obterExtremos(){
	let extremos = {},
	    diaInicio = dataInicio.substr(0,2),
	    mesInicio = dataInicio.substr(3,2),
	    anoInicio = dataInicio.substr(6,4),
	    diaFim = dataFim.substr(0,2),
	    mesFim = dataFim.substr(3,2),
	    anoFim = dataFim.substr(6,4),
	    dataHoje = new Date(),
	    dataInicioGrafico,
	    dataFimGrafico;

	//verifica se 'dataInicio' corresponde ao dia de hoje
	if (
		diaInicio == dataHoje.getDate() 
		&& mesInicio == dataHoje.getMonth() + 1 
		&& anoInicio == dataHoje.getFullYear()
	){
		dataInicioGrafico = (new Date(
					anoInicio,
					mesInicio - 1,
					diaInicio,
					4
				     )).getTime();
		dataFimGrafico = (new Date(
					anoFim,
					mesFim - 1,
					diaInicio,
					(new Date()).getHours() - 2
				 )).getTime();

	}
	else{
		dataInicioGrafico = (new Date(
					anoInicio,
					mesInicio - 1,
					diaInicio,
					4
				     )).getTime();
		dataFimGrafico = (new Date(
					anoFim,
					mesFim - 1,
					diaFim,
					19,59,59
				     )).getTime();
		
	}	
	

	extremos.inicio = dataInicioGrafico;
	extremos.fim = dataFimGrafico;

	return extremos;
}


//ATUALIZAR EXTREMOS DO GRÁFICO: setExtremes()
//PENSAR EM MODIFICAR FORMATO PADRÃO DAS DATAS UTILIZADAS NAS VARIÁVEIS GLOBAIS PARA GUARDAREM OBJETOS DA CLASSE Date
function inicializarGrafico(idContainer,stringArrayNiveis){
	let arrayNiveis = eval(stringArrayNiveis),
	    extremos = obterExtremos();
	
	if (grafico){
		//grafico já foi inicializado
		grafico.setTitle({text: codigosEstacoes[codigoAtualEstacaoANA]},false);
		grafico.xAxis[0].setExtremes(extremos.inicio,extremos.fim,false);
		grafico.series[0].update({
			pointStart: extremos.inicio,
			data: arrayNiveis
		},false);
		grafico.redraw();
	}
	else{
		//grafico ainda não foi inicializado
		grafico = Highcharts.chart(idContainer, {
		    title: {
			text: 'Nível da Bacia - ' + codigosEstacoes[codigoAtualEstacaoANA]
		    },

		    subtitle: {
			text: 'Fonte: Agência Nacional de Águas - ANA'
		    },

		    yAxis: {
			title: {
			    text: 'Nível(cm)'
			},
			
		    },
		     xAxis: {
			type: 'datetime'
		    },
		    legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		    },
			
		    plotOptions: {
		    line: {
			    dataLabels: {
				enabled: true
			    },
			    enableMouseTracking: false,
			    pointInterval: 900000, 
			    pointStart: extremos.inicio
 			}
		    },

		    series: [{
			name: 'Níveis',
			data: arrayNiveis
		    }]
		});

		}

}

