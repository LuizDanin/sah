<?php
header('Content-Type: text/html; charset=ISO-8859-15');
require_once 'AnaXML.php';
//require_once 'funcoes.php';
# An HTTP GET request example
$date = new DateTime();
$dataAtual = $date->format('d/m/Y');
$estacao = $_GET['estacao'];
$dataInicio = $_GET['dataInicio'];
$dataFim = $_GET['dataFim'];

if(date('w') == 1)
{
	$diaAnteriorInicial = new DateTime();
	$diaAnteriorInicial->modify('-3 day');
	$diaAnteriorInicial = $diaAnteriorInicial->format('d/m/Y');

	$diaAnteriorFinal = new DateTime();
	$diaAnteriorFinal->modify('-1 day');
	$diaAnteriorFinal = $diaAnteriorFinal->format('d/m/Y');
}
else
{
	$diaAnteriorInicial = new DateTime();
	$diaAnteriorInicial->modify('-1 day');
	$diaAnteriorInicial = $diaAnteriorInicial->format('d/m/Y');

	$diaAnteriorFinal = new DateTime();
	$diaAnteriorFinal->modify('-1 day');
	$diaAnteriorFinal = $diaAnteriorFinal->format('d/m/Y');
}

$diaInicialMes = date('01/m/Y');
//$diaInicialMes = $diaInicialMes->format('d/m/Y');


//$url = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$dataAtual.'&dataFim='.$dataAtual;
$urlDataAtual = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$dataAtual.'&dataFim='.$dataAtual;
$urlIntervalo = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$dataInicio.'&dataFim='.$dataFim;

$urlx = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$diaAnteriorInicial.'&dataFim='.$diaAnteriorFinal;	

$urly = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.$estacao.'&dataInicio='.$diaInicialMes.'&dataFim='.$diaFinal;

	
#dados estacao dia atual
$dataXml = AnaXML::getEstacoesAna($urlDataAtual);
$xml = AnaXML::getAjusteXML($dataXml);
$arrayEstacao = AnaXML::getDadosHidrometeorologicosByEstacao($xml);

#dados estacao intervalo de data
$dataXml = AnaXML::getEstacoesAna($urlIntervalo);
$xml = AnaXML::getAjusteXML($dataXml);
#precipita��o do intervalo dado
$precipitacaoIntervalo = AnaXML::getPrecipitacao($xml);

//$arrayEstacaoAltamira = $funcoes->arrayEstacao('altamira');

#precipitacao dia anterior
$dataXmlAnterior = AnaXML::getEstacoesAna($urlx);
$xmlAnterior = AnaXML::getAjusteXML($dataXmlAnterior);
$precipitacaoDiaAnterior = AnaXML::getPrecipitacao($xmlAnterior);

#precipitacao acumulada do mes
$dataXmlAcumulada = AnaXML::getEstacoesAna($urly);
$xmlAcumulada = AnaXML::getAjusteXML($dataXmlAcumulada);
$precipitacaoAcumulada = AnaXML::getPrecipitacao($xmlAcumulada);

if ($arrayEstacao->CodEstacao == ""){
	
echo"<h4 style='color:red;'>Esta��o em Manuten��o</h4>";
}else{
	if ($dataInicio == $dataFim)
		$intervaloData = $dataInicio;
	else
		$intervaloData = $dataInicio.' ao '.$dataFim;
	echo"<p><strong>Codigo Fluviometrico da Esta��o: </strong>$arrayEstacao->CodEstacao</p>
		<p><strong>N�vel para �s 07:00h do dia Atual: </strong> $arrayEstacao->Nivel</p>
		<p><strong>Chuva do dia $intervaloData: </strong> $precipitacaoIntervalo</p>
		<p><strong>Chuva do dia anterior: </strong> $precipitacaoDiaAnterior</p>
		<p><strong>Chuva acumulada (no m�s): </strong>$precipitacaoAcumulada</p>";
}

?>
